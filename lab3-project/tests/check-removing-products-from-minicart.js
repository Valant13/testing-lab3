const config = require('../config');
const data = {
    productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
    productQuantity: '0'
};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(data.productPageUrl);

    let addProductButton = await driver.findElement(By.id('product-addtocart-button'));
    await addProductButton.click();

    await driver.sleep(config.pageLoadTime);

    let minicartButton = (await driver.findElements(By.className('action showcart')))[0];
    await minicartButton.click();

    let removeProductButton = (await driver.findElements(By.className('action delete')))[0];
    await removeProductButton.click();

    await driver.sleep(config.pageLoadTime);

    let confirmButton = (await driver.findElements(By.className('action-primary action-accept')))[0];
    await confirmButton.click();

    await driver.sleep(config.pageLoadTime);

    let counterElement = (await driver.findElements(By.className('counter-number')))[0];
    let counterValue = await counterElement.getText();

    if (counterValue === data.productQuantity) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);
        console.log(`Expected counter value: ${data.productQuantity}`);
        console.log(`Actual counter value: ${counterValue}`);
    }

    await driver.quit();
}
