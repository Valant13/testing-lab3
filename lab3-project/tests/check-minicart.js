const config = require('../config');
const data = {};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(config.websiteUrl);

    await driver.sleep(config.pageLoadTime * 2);

    let minicartButton = (await driver.findElements(By.className('action showcart')))[0];
    let minicartElement = await driver.findElement(By.id('ui-id-1'));

    await minicartButton.click();
    let isMinicartAppear = await minicartElement.isDisplayed();

    await minicartButton.click();
    let isMinicartDisappear = !(await minicartElement.isDisplayed());

    if (isMinicartAppear && isMinicartDisappear) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);

        if (!isMinicartAppear) {
            console.log('Minicart is not shown');
        }

        if (!isMinicartDisappear) {
            console.log('Minicart is not hidden');
        }
    }

    await driver.quit();
}
