const config = require('../config');
const data = {
    searchText: 'Ring'
};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(config.websiteUrl);

    let searchInput = await driver.findElement(By.id('search'));
    await searchInput.sendKeys(data.searchText);

    await driver.sleep(config.pageLoadTime);

    let searchButton = (await driver.findElements(By.className('action search')))[0];
    await searchButton.click();

    let urlString = await driver.getCurrentUrl();
    let url = new URL(urlString);
    let urlSearchText = url.searchParams.get('q');

    if (urlSearchText === data.searchText) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);
        console.log(`Expected search text: ${data.searchText}`);
        console.log(`Actual search text: ${urlSearchText}`);
    }

    await driver.quit();
}
