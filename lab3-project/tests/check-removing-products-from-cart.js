const config = require('../config');
const data = {
    productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
    cartPageUrl: config.websiteUrl + 'checkout/cart/',
    productQuantity: '0'
};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(data.productPageUrl);

    let addProductButton = await driver.findElement(By.id('product-addtocart-button'));
    await addProductButton.click();

    await driver.sleep(config.pageLoadTime);

    let removeProductButton = (await driver.findElements(By.className('action action-delete')))[0];
    await removeProductButton.click();

    await driver.sleep(config.pageLoadTime);

    let counterElement = (await driver.findElements(By.className('counter-number')))[0];
    let counterValue = await counterElement.getText();

    let urlString = await driver.getCurrentUrl();

    if (urlString === data.cartPageUrl && counterValue === data.productQuantity) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);

        if (urlString !== data.cartPageUrl) {
            console.log(`Expected search text: ${data.cartPageUrl}`);
            console.log(`Actual search text: ${urlString}`);
        }

        if (counterValue !== data.productQuantity) {
            console.log(`Expected counter value: ${data.productQuantity}`);
            console.log(`Actual counter value: ${counterValue}`);
        }
    }

    await driver.quit();
}
