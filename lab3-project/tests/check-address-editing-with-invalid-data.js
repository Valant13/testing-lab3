const config = require('../config');
const data = {
    loginPageUrl: config.websiteUrl + 'customer/account/login/',
    addressBookUrl: config.websiteUrl + 'customer/address/index/',
    addressEditPageUrl: config.websiteUrl + 'customer/address/edit/'
};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(data.loginPageUrl);

    let emailInput = await driver.findElement(By.id('email'));
    emailInput.sendKeys(config.email);

    let passwordInput = await driver.findElement(By.id('pass'));
    passwordInput.sendKeys(config.password);

    let loginButton = (await driver.findElements(By.className('action login primary')))[0];
    await loginButton.click();

    await driver.get(data.addressBookUrl);

    let addressSection = (await driver.findElements(By.className('box box-address-billing')))[0];
    let editAddressButton = (await addressSection.findElements(By.className('action edit')))[0];
    await editAddressButton.click();

    await driver.sleep(config.pageLoadTime);

    let phoneNumberInput = await driver.findElement(By.id('telephone'));
    await phoneNumberInput.clear();

    let saveButton = (await driver.findElements(By.className('action save primary')))[0];
    await saveButton.click();

    let urlString = await driver.getCurrentUrl();

    if (urlString.includes(data.addressEditPageUrl)) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);
        console.log(`Expected page URL: ${data.addressEditPageUrl}`);
        console.log(`Actual page URL: ${urlString}`);
    }

    await driver.quit();
}
