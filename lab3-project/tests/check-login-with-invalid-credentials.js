const config = require('../config');
const data = {
    loginPageUrl: config.websiteUrl + 'customer/account/login/',
    fakePassword: 'fakepass'
};

const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

main(driver);

async function main(driver) {
    await driver.get(data.loginPageUrl);

    let emailInput = await driver.findElement(By.id('email'));
    emailInput.sendKeys(config.email);

    let passwordInput = await driver.findElement(By.id('pass'));
    passwordInput.sendKeys(data.fakePassword);

    let loginButton = (await driver.findElements(By.className('action login primary')))[0];
    await loginButton.click();

    let urlString = await driver.getCurrentUrl();

    if (urlString === data.loginPageUrl) {
        console.log(config.successMessage);
    } else {
        console.log(config.failMessage);
        console.log(`Expected page URL: ${data.loginPageUrl}`);
        console.log(`Actual page URL: ${urlString}`);
    }

    await driver.quit();
}
